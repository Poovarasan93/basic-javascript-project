const quizData = [
  {
    question: "Which language runs in a web browser?",
    a: "Java",
    b: "C",
    c: "Python",
    d: "JavaScript",
    correct: "d",
  },
  {
    question: "What does CSS stand for?",
    a: "Central Style Sheets",
    b: "Cascading Style Sheets",
    c: "Cascading Simple Sheets",
    d: "Cars SUVs Sailboats",
    correct: "b",
  },
  {
    question: "What does HTML stand for?",
    a: "Hypertext Markup Language",
    b: "Hypertext Markdown Language",
    c: "Hyperloop Machine Language",
    d: "Helicopters Terminals Motorboats Lamborginis",
    correct: "a",
  },
  {
    question: "What year was JavaScript launched?",
    a: "1996",
    b: "1995",
    c: "1994",
    d: "none of the above",
    correct: "d",
  },
];

var index=0;
var correct=0;
var worng=quizData.length;

function evaluateAnswer(){
  var answer=document.querySelector('input[name="answer"]:checked');
  if(!answer){
    return  alert("please select option");
 }
  var correctValue=quizData[index].correct;
  
  if(correctValue === answer.value){
    correct++;
    worng--;
  }
  document.getElementById("correctAns").textContent=correct;
  document.getElementById("worngAns").textContent=worng;
}

document.getElementById("submit").addEventListener('click', evaluateAnswer);

document.getElementById("next").addEventListener('click', function() {
  evaluateAnswer();
    index++;
  if(quizData.length -1 === index){
    document.getElementById("next").classList.add("d-none");
    document.getElementById("reset").classList.remove("d-none");
    document.getElementById("submit").classList.remove("d-none");
  }
  showQuestions();

});
document.getElementById("reset").addEventListener('click', function() {
    index=0;
    correct=0;
    worng=quizData.length;
    document.getElementById("next").classList.remove("d-none");
    document.getElementById("reset").classList.add("d-none");
    document.getElementById("submit").classList.add("d-none");
  showQuestions();
});
function showQuestions() {
  let task = "";
    task = `
    <div class="quiz-header p-2">
    <h3>${quizData[index].question}</h3>
   </div>
   <div class="form-check ">
    <input class="form-check-input" type="radio" value="a" name="answer" id="flexRadioDefault1">
    <label class="form-check-label" for="flexRadioDefault1">
      ${quizData[index].a}
    </label>
  </div>
  <div class="form-check ">
    <input class="form-check-input" type="radio" value="b" name="answer" id="flexRadioDefault1">
    <label class="form-check-label" for="flexRadioDefault1">
    ${quizData[index].b}
    </label>
  </div>
  <div class="form-check ">
    <input class="form-check-input" type="radio" value="c" name="answer" id="flexRadioDefault1">
    <label class="form-check-label" for="flexRadioDefault1">
    ${quizData[index].c}
    </label>
  </div>
  <div class="form-check ">
    <input class="form-check-input" type="radio" value="d" name="answer" id="flexRadioDefault1">
    <label class="form-check-label" for="flexRadioDefault1">
    ${quizData[index].d}
    </label>
  </div>`;
  document.getElementById("list").innerHTML = task;
}
showQuestions();







