
document.getElementById("calculate").addEventListener("click",submit);
document.getElementById("reset").addEventListener("click",reset);
document.getElementById("name").onchange = submit;
document.getElementById("email").onchange = submit;
document.getElementById("message").onchange = submit;


function submit(event){
    event.preventDefault();
    const name =  document.getElementById("name");
    const email =  document.getElementById("email");
    const area = document.getElementById("message");
    const attrname = name.getAttribute("name");
    const attrEmail =email.getAttribute("name");
    const attrMessage = area.getAttribute("name");
    if(name.value !== ""){
    localStorage.setItem(attrname, name.value);
    }
    if(email.value !== ""){
        localStorage.setItem(attrEmail, email.value);
    }
    if(area.value !== ""){
        localStorage.setItem(attrMessage, area.value);
    }
   
}
function reset(){
    const name =  document.getElementById("name").value="";
    const email =  document.getElementById("email").value="";
    const area = document.getElementById("message").value="";
    
   
}