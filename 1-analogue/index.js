

var canvas = document.querySelector(".canvas");
var draw = canvas.getContext("2d");
var radius= canvas.height/ 2;
draw.translate(radius,radius);
radius = radius * 0.90;

setInterval(drawclock,1000)
function drawclock(){
        drawClockFace();
        drawNumbers()
        drawTime();
    
}


function drawClockFace(){
    let grad ;
    draw.beginPath();
    draw.arc(0,0,radius,0,2*Math.PI)
    draw.fillStyle ="white";
    draw.fill(); 
    grad =draw.createRadialGradient(0,0,radius*0.95,0,0,radius*1.05);
    grad.addColorStop(0,"#eb0ea8");
    grad.addColorStop(0.5,"#eb0ea8");
    grad.addColorStop(1,"#eb0ea8");
    draw.strokeStyle = grad;
    draw.lineWidth=radius*0.1;
    draw.stroke();
    draw.beginPath();
    draw.arc(0,0,radius*0.1,0,2*Math.PI);
    draw.fillStyle ="#333";
    draw.fill();
    
}


function drawNumbers(){
        var ang ;
        var num ;
        draw.font = radius*0.15 +"px arial";
        draw.textBaseline ="middle";
        draw.textAlign="center";
        for(num=1;num<13;num++){
            ang= num*Math.PI /6;
            draw.rotate(ang);
            draw.translate(0,-radius*0.85);
            draw.rotate(-ang);
            draw.fillText(num.toString(),0,0);
            draw.rotate(ang);
            draw.translate(0,radius*0.85);
            draw.rotate(-ang);
            console.log(num)
        
        }
}


function drawTime(ctx,radiu){
            var now = new Date;
            var hour = now.getHours();
            var mintue = now.getMinutes();
            var second = now.getSeconds();
            hour = hour % 12;
            hour = (hour*Math.PI/6)+
            (mintue*Math.PI/(6*60))+
            (second*Math.PI/(360*60));
            
            drawHand(draw,hour,radius*0.5,radius*0.07);
            mintue = (mintue*Math.PI/30)+(second*Math.PI/(30*60));
            drawHand(draw,mintue,radius*0.8,radius*0.07);
            second = (second*Math.PI/30);
            drawHand(draw,second,radius*0.9,radius*0.02);

}
function drawHand(draws,pos,length,width){
            draws.beginPath();
            draws.lineWidth= width;
            draws.lineCap = "round";
            draws.moveTo(0,0);
            draws.rotate(pos);
            draws.lineTo(0,-length);
            draws.stroke();
            draws.rotate(-pos);
}