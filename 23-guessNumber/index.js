

let secretNumber = Math.trunc(Math.random() * 20) + 1;
let numberOfGuess=[];
let maxtry=3;


const displayMessage = function (message) {
  document.querySelector('.message').textContent = message;
};

document.querySelector('#check').addEventListener('click', function () {
  const guess = Number(document.getElementById("number").value);

  if (!guess) {
    displayMessage('No number!');

  } else if (guess === secretNumber && (maxtry > 0)) {
    displayMessage('Correct Number!');
   

  } else if (guess !== secretNumber) {
    numberOfGuess.push(guess);
    if (maxtry > 0) {
      maxtry--;
     document.querySelector('#preGuess').textContent = numberOfGuess.toString();
     document.querySelector('#prvList').classList.remove("d-none");
      displayMessage(guess > secretNumber ? 'Too high!' : ' Too low!');
      
    } else {
      document.querySelector('#preGuess').textContent = "";
      numberOfGuess=[];
      displayMessage('You lost the game!');
    }
  }

  
});

function reset(){
  document.querySelector('#preGuess').textContent = "";
  numberOfGuess=[];
  secretNumber = Math.trunc(Math.random() * 20) + 1;
  document.querySelector('#prvList').classList.add("d-none");
  document.querySelector('.message').textContent = "";
  document.getElementById("number").value="";
  displayMessage('start Guessing');
}
displayMessage('start Guessing');

